## Detektivky
- **[Krycí barva](https://www.databazeknih.cz/knihy/kryci-barva-50318)** - Charles Percy Snow
- **[Křížová palba](https://www.databazeknih.cz/knihy/rychlopalba-krizova-palba-423672)** - Štěpán Kopřiva - (12/2020)
- **[Rychlopalba](https://www.databazeknih.cz/knihy/rychlopalba-rychlopalba-239247)** - Štěpán Kopřiva - (10/2021)
- **[Sudetenland](https://www.databazeknih.cz/knihy/sudetenland-518323)** - Leoš Kyša - (12/2023)
- **[Pouť oběšenců](https://www.databazeknih.cz/knihy/pout-obesencu-417763)** - Pavel Hrdlička - (01/2024)
- **[Smrt nosí rudé škorně](https://www.databazeknih.cz/knihy/podrychtar-vaclav-smrt-nosi-rude-skorne-136763)** - Pavel Hrdlička - (10/2024)

## Fantasy
- **[Američtí bohové](https://www.databazeknih.cz/knihy/americti-bohove-americti-bohove-1188)** - Neil Gaiman (11/2021)
- **[Bakly - Armáda](https://www.databazeknih.cz/knihy/bakly-armada-426923)** - Miroslav Žamboch (06/2023)
- **[Nikdykde](https://www.databazeknih.cz/knihy/nikdykde-1179)** - Neil Gaiman
- **[Oceán na konci uličky](https://www.databazeknih.cz/knihy/ocean-na-konci-ulicky-172347)** - Neil Gaiman (1/2021)
- **[Devatar sedr](https://www.databazeknih.cz/knihy/devatar-sedr-3068)** - Pavel a Petr Harašímovi
- **[Perunova krev I.](https://www.databazeknih.cz/knihy/perunova-krev-perunova-krev-i-179587)** - František Kotleta
- **[Perunova krev II.](https://www.databazeknih.cz/knihy/perunova-krev-perunova-krev-ii-204194)** - František Kotleta
- **[Příliš dlouhá swingers párty](https://www.databazeknih.cz/knihy/tomas-kosek-prilis-dlouha-swingers-party-227345)** - František Kotleta (2018)
- **[Underground](https://www.databazeknih.cz/knihy/underground-436924)** - František Kotleta
- **[Vlci](https://www.databazeknih.cz/knihy/bratrstvo-krve-vlci-272455)** - František Kotleta (3/2018)
- **[Denní hlídka](https://www.databazeknih.cz/knihy/hlidka-denni-hlidka-4729)** - Sergej Lukjaněnko (3/2018)
- **[Noční hlídka](https://www.databazeknih.cz/knihy/hlidka-nocni-hlidka-499)** - Sergej Lukjaněnko (11/2017)
- **[Tance na sněhu](https://www.databazeknih.cz/knihy/genom-tance-na-snehu-261255)** - Sergej Lukjaněnko (2017)
- **[Barva kouzel](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-barva-kouzel-452)** - Terry Pratchett
- **[Čarodějky na cestách](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-carodejky-na-cestach-209323)** - Terry Pratchett
- **[Erik](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-erik-460)** - Terry Pratchett
- **[Magický prazdroj](https://www.databazeknih.cz/knihy/magicky-prazdroj-196063)** - Terry Pratchett
- **[Pyramidy](https://www.databazeknih.cz/knihy/pyramidy-69561)** - Terry Pratchett
- **[Stráže! Stráže!](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-straze-straze-69565)** - Terry Pratchett
- **[Sekáč](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-sekac-285977)** - Terry Pratchett
- **[Těžké melodično](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-tezke-melodicno-467)** - Terry Pratchett (2022)
- **[Úžasný Mauric a jeho vzdělaní hlodavci](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-uzasny-mauric-a-jeho-vzdelani-hlodavci-185908)** - Terry Pratchett
- **[Zajímavé časy](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-zajimave-casy-468)** - Terry Pratchett (10/2021)
- **[Zaklínač - I. Poslední přání](https://www.databazeknih.cz/knihy/posledni-prani-30396)** - Andrzej Sapkowski (11/2022)
- **[Zaklínač - II. Meč osudu](https://www.databazeknih.cz/knihy/mec-osudu-328737)** - Andrzej Sapkowski (12/2022)
- **[Zloděj času](https://www.databazeknih.cz/knihy/uzasna-zemeplocha-uzasna-plochozem-zlodej-casu-477)** - Terry Pratchett (11/2009)
- **[Živí a mrtví](https://www.databazeknih.cz/knihy/zivi-a-mrtvi-114856)** - Miroslav Žamboch
- **[Krev pro rusalku](https://www.databazeknih.cz/knihy/krev-pro-rusalku-390163)** - Kristýna Sněgoňová (01/2023)
- **[Zřídla](https://www.databazeknih.cz/knihy/zridla-419689)** - Kristýna Sněgoňová (05/2023)
- **[Lockdown](https://www.databazeknih.cz/knihy/lockdown-468416)** - František Kotleta, Kristýna Sněgoňová, Roman Bureš, Michaela Merglová, Dalibor Vácha a kolektiv (02/2023)
- **[Jak lvové](https://www.databazeknih.cz/knihy/fastynger-van-hauten-jak-lvove-266645)** - Petr Schink (02/2023)
- **[Říše](https://www.databazeknih.cz/knihy/rise-437052)** - Roman Bureš (05/2023)
- **[Inferium](https://www.databazeknih.cz/knihy/inferium-inferium-403761)** - Roman Bureš (06/2023)
- **[Vítejte v pekle](https://www.databazeknih.cz/knihy/vitejte-v-pekle-504940)** - Roman Bureš (08/2023)
- **[Temné blues v New Orleans](https://www.atabazeknih.cz/knihy/tomas-kosek-temne-blues-v-new-orleans-505000)** - František Kotleta (6/2023)
- **[Inferium II - Invaze](https://www.databazeknih.cz/knihy/inferium-invaze-492317)** - Roman Bureš (01/2024)
- **[Asfalt](https://www.databazeknih.cz/knihy/asfalt-9659) - Štěpán Kopřiva (03/2024)**

## Sci-fi
- **[Hosté z planety lidí](https://www.databazeknih.cz/knihy/hoste-z-planety-lidi-21392)** - kolektiv autorů - sestavil Karel Blažek
- **[Lety zakázanou rychlostí](https://www.databazeknih.cz/knihy/lety-zakazanou-rychlosti-86114)** - kolektiv autorů - sestavil Pavel Kosatík
- **[Aktivní kovy](https://www.databazeknih.cz/knihy/aktivni-kovy-222608)** - Štěpán Kopřiva
- **[Bludiště odrazů](https://www.databazeknih.cz/knihy/hlubina-bludiste-odrazu-494)** - Sergej Lukjaněnko
- **[Konkurenti](https://www.databazeknih.cz/knihy/konkurenti-144746)** - Sergej Lukjaněnko
- **[Lživá zrcadla](https://www.databazeknih.cz/knihy/hlubina-lziva-zrcadla-493)** - Sergej Lukjaněnko
- **[Poslední tango v Havaně](https://www.databazeknih.cz/knihy/tomas-kosek-posledni-tango-v-havane-330799)** - František Kotleta
- **[Spad](https://www.databazeknih.cz/knihy/spad-spad-296894)** - František Kotleta
- **[Spad *(souborné vydání)*](https://www.databazeknih.cz/knihy/spad-spad-souborne-vydani-504738)** - František Kotleta (02/2024)
- **[Velké problémy v malém Vietnamu](https://www.databazeknih.cz/knihy/tomas-kosek-velke-problemy-v-malem-vietnamu-305714)** - František Kotleta
- **[Operace Thümmel](https://www.databazeknih.cz/knihy/legie-operace-thummel-453774)** - František Kotleta (12/2022)
- **[Amanda](https://www.databazeknih.cz/knihy/legie-amanda-454576)** - Kristýna Sněgoňová (12/2022)
- **[Šprti & frajeři](https://www.databazeknih.cz/knihy/legie-sprti-aamp-frajeri-469601)** - Kristýna Sněgoňová (12/2022)
- **[Rudý vrabčák](https://www.databazeknih.cz/knihy/legie-rudy-vrabcak-483473)** - František Kotleta (12/2022)
- **[Aga](https://www.databazeknih.cz/knihy/legie-aga-493238)** - František Kotleta (12/2022)
- **[Pán hor](https://www.databazeknih.cz/knihy/legie-pan-hor-493240)** - Kristýna Sněgoňová (02/2023)
- **[Mrtvá schránka](https://www.databazeknih.cz/knihy/legie-mrtva-schranka-506913)** - Kristýna Sněgoňová (3/2023)
- **[Operace Petragun](https://www.databazeknih.cz/knihy/legie-operace-petragun-510247)** - František Kotleta (7/2023)
- **[Na konci vesmíru](https://www.databazeknih.cz/knihy/legie-na-konci-vesmiru-522889)** - Kristýna Sněgoňová (11/2023)
- **[Plameny války](https://www.databazeknih.cz/knihy/legie-plameny-valky-536204)** - František Kotleta (10/2024)
- **[Mirská ruleta](https://www.databazeknih.cz/knihy/legie-mirska-ruleta-548439)** - Kristýna Sněgoňová (12/2024)
- **[Mluvčí za mrtvé](https://www.databazeknih.cz/knihy/enderova-saga-mluvci-za-mrtve-12806)** - Orson Scott Card
- **[Enderova hra](https://www.databazeknih.cz/knihy/enderova-saga-enderova-hra-30553)** - Orson Scott Card (6/2016)
- **[Bajky robotů](https://www.databazeknih.cz/knihy/bajky-robotu-13772)** - Stanislaw Lem
- **[Budoucnost](https://www.databazeknih.cz/knihy/budoucnost-260083)** - Dmitry Glukhovsky (2017)
- **[Metro 2033](https://www.databazeknih.cz/knihy/metro-metro-2033-33191)** - Dmitry Glukhovsky
- **[Metro 2035](https://www.databazeknih.cz/knihy/metro-metro-2035-255853)** - Dmitry Glukhovsky (2017)
- **[Flotila Země](https://www.databazeknih.cz/knihy/flotila-zeme-28016)** - Patrik Zandl
- **[Genom](https://www.databazeknih.cz/knihy/genom-genom-261256)** - Sergej Lukjaněnko (1/2018)
- **[Stráž](https://www.databazeknih.cz/knihy/pohranici-straz-301981)** - Sergej Lukjaněnko (4/2018)
- **[Kosmické proudy](https://www.databazeknih.cz/knihy/galakticka-rise-trantorske-imperium-kosmicke-proudy-21662)** - Isaac Asimov (2/2018)
- **[Nahé slunce](https://www.databazeknih.cz/knihy/romany-o-robotech-nahe-slunce-2315)** - Isaac Asimov (2018)
- **[Počítačoví piráti](https://www.databazeknih.cz/knihy/pocitacovi-pirati-124392)** - kolektiv autorů - Isaac Asimov (2018)
- **[Stopařův průvodce Galaxií](https://www.databazeknih.cz/knihy/stoparuv-pruvodce-galaxii-stoparuv-pruvodce-galaxii-3996)** - Douglas Adams (2018)
- **[Virtuální vrazi](https://www.databazeknih.cz/knihy/virtualni-vrazi-259315)** - Jana Rečková (07/2022)
- **[Ve stínu apokalypsy](https://www.databazeknih.cz/knihy/ve-stinu-ve-stinu-apokalypsy-401720)** - František Kotleta & Boris Hokr(01/2023)
- **[Propast času](https://www.databazeknih.cz/knihy/propast-casu-propast-casu-269697)** - Roman Bureš (02/2023)
- **[Císařovna](https://www.databazeknih.cz/knihy/propast-casu-cisarovna-310156)** - Roman Bureš (03/2023)
- **[Impérium](https://www.databazeknih.cz/knihy/propast-casu-imperium-360522)** - Roman Bureš (05/2023)
- **[Město v oblacích](https://www.databazeknih.cz/knihy/mesto-v-oblacich-444756)** - Kristýna Sněgoňová (09/2023)
- **[Země v troskách](https://www.databazeknih.cz/knihy/zeme-v-troskach-481168)** - Kristýna Sněgoňová (10/2023)
- **[Svět v bouři](https://www.databazeknih.cz/knihy/mesta-kristyna-snegonova-svet-v-bouri-496830)** - Kristýna Sněgoňová (10/2023)
- **[Hlubiny města](https://www.databazeknih.cz/knihy/hlubiny-mesta-513069)** - antologie, Kristýna Sněgoňová, Lukáš Vavrečka (12/2023)
- **[Válkotvůrci](https://www.databazeknih.cz/knihy/valkotvurci-245615)** - František Kotleta - (03/2024)
- **[Černá smečka](https://www.databazeknih.cz/prehled-knihy/tobiasuv-rad-cerna-smecka-522337)** - František Kotleta - (12/2024)

## Beletrie
- **[Bílá nemoc](https://www.databazeknih.cz/knihy/bila-nemoc-687)** - Karel Čapek
- **[Cesta na sever](https://www.databazeknih.cz/knihy/cesta-na-sever-686)** - Karel Čapek (7/2010)
- **[Hlídač č. 47](https://www.databazeknih.cz/knihy/hlidac-c-47-113)** - Josef Kopta (6/2009)
- **[Kniha apokryfů](https://www.databazeknih.cz/knihy/kniha-apokryfu-15155)** - Karel Čapek (7/2010)
- **[Krakatit](https://www.databazeknih.cz/knihy/krakatit-688)** - Karel Čapek (2/2011)
- **[Obyčejný život](https://www.databazeknih.cz/knihy/obycejny-zivot-16434)** - Karel Čapek (2018)
- **[Povětroň](https://www.databazeknih.cz/knihy/povetron-26613)** - Karel Čapek (2019)
- **[Stín kapradiny](https://www.databazeknih.cz/knihy/stin-kapradiny-5645)** - Josef Čapek (2019)
- **[Továrna na Absolutno](https://www.databazeknih.cz/knihy/tovarna-na-absolutno-515)** - Karel Čapek
- **[Opustíš-li mne, nezahynu](https://www.databazeknih.cz/knihy/opustis-li-mne-nezahynu-337694)** - Viktor Dyk (6/2009)
- **[Podél cesty](https://books.google.cz/books/about/Pod%C3%A9l_cesty.html?id=N3TnAAAAMAAJ&redir_esc=y)** - Viktor Dyk (2/2011)
- **[Soumrak](https://www.databazeknih.cz/knihy/soumrak-146585)** - Dmitry Glukhovsky (2016)
- **[Děravý úřad](https://www.databazeknih.cz/knihy/deravy-urad-54123)** - David Langford (11/2009)
- :heart: **[Rekvalifikační kurz](https://www.databazeknih.cz/knihy/rekvalifikacni-kurz-86627)** - Ondřej Neff (2017)
- **[Pan Kaplan má stále třídu rád](https://www.databazeknih.cz/knihy/pan-kaplan-ma-stale-tridu-rad-35511)** - Leo Calvin Rosten (11/2009)
- **[Výlet do hor](https://www.databazeknih.cz/knihy/vylet-do-hor-142364)** - Roger Vailland (5/2022)
- **[Legionář](https://www.databazeknih.cz/knihy/legionar-174263)** - Roman Bureš (6/2023)
- **[Leon](https://www.databazeknih.cz/knihy/leon-402435)** - Roman Bureš (7/2023)
- **[Lupiči nedobytných pokladen](https://www.databazeknih.cz/knihy/lupici-nedobytnych-pokladen-262586)** - (02/2024)

## Romány
- **[Čas žít, čas umírat](https://www.databazeknih.cz/prehled-knihy/cas-zit-cas-umirat-533)** 1. vydání - Erich Maria Remarque (05/2024
- **[Cesta zpátky](https://www.databazeknih.cz/prehled-knihy/cesta-zpatky-350)** 1. vydání - Erich Maria Remarque (06/2024

## Thriller
- :heart: **[101 minut](https://www.databazeknih.cz/knihy/101-minut-488962)** - Štěpán Kopřiva - (9/2022)

## Strava
- **[Hmyz na talíři](https://www.databazeknih.cz/knihy/hmyz-na-taliri-268617)** - Marie Borkovcová (9/2016)
- **[Život bez pšenice](https://www.databazeknih.cz/knihy/zivot-bez-psenice-zivot-bez-psenice-170089)** - William Davis

## Sport
- **[Plavání](https://www.databazeknih.cz/knihy/plavani-174233)** - Terry Laughlin (7/2016)
- **[Zrozeni k běhu](https://www.databazeknih.cz/knihy/born-to-run-zrozeni-k-behu-born-to-run-zrozeni-k-behu-216142)** - Christoper McDougall
- **[Můj dlouhý běh](https://www.databazeknih.cz/knihy/muj-dlouhy-beh-211193)** - Daniel Orálek (2017)
- **[Bosé běhání](https://www.databazeknih.cz/knihy/bose-behani-237399)** - Michael Sandler (12/2016)
- **[Škorpilova škola běhu](https://www.databazeknih.cz/knihy/skorpilova-skola-behu-198560)** - Miloš Škorpil (12/2016)

## Letectví
- **[Letadlo, můj osud](https://www.databazeknih.cz/knihy/skorpilova-skola-behu-198560)** - Jan Čech (2017)
- **[Příběhy zpod oblak](https://knihy.abz.cz/prodej/pribehy-zpod-oblak)** - Vladimír Charousek (10/2012)
- **[Do větru a mraků](https://www.databazeknih.cz/knihy/do-vetru-a-mraku-119747)** - Oldřich Měrka (10/2012)
- **[Paragliding](https://www.databazeknih.cz/knihy/paragliding-paragliding-111277)** - Richard Plos a kolektiv
- **[Vrtulníky](https://www.databazeknih.cz/knihy/vrtulniky-60318)** - Václav Svoboda

## Osobní rozvoj
- **[Mít vše hotovo](https://www.databazeknih.cz/knihy/mit-vse-hotovo-298563)** - David Allen (1/2010)
- **[Šťastný jako Dán](https://www.databazeknih.cz/knihy/stastny-jako-dan-374641)** - Malene Rydahl (2018)

## Cizojazyčné
- **[How to be a little sod](https://www.goodreads.com/book/show/1230968.How_to_Be_a_Little_Sod)** - Simon Brett (2/2014)

## Počítače
- **[Ansible for DevOps](https://www.ansiblefordevops.com/)** - Jeff Geerling
- **[UNIX V - příručka správce systému](https://search.mlp.cz/cz/titul//54020/#/getPodobneTituly=deskriptory-eq:6119-amp:key-eq:54020)** - Jiří Hnát
- **JNCIA-Junos Study Guide—Part 1** - Juniper networks
- **JNCIA-Junos Study Guide—Part 2** - Juniper networks
- **[TCP/IP v kostce](https://www.databazeknih.cz/knihy/tcp-ip-v-kostce-168388)** - Rita Pužmanová (12/2009)
- **[CCNA](https://search.mlp.cz/cz/titul//2847624/#/dk=key-eq:4191119-amp:titul-eq:true&getPodobneTituly=deskriptory-eq:97604239-amp:key-eq:2847624)** - Todd Lammle (2011)
